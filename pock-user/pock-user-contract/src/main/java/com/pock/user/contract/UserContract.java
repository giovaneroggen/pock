package com.pock.user.contract;

import com.pock.user.contract.request.UserRequest;
import com.pock.user.contract.response.UserResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Seven on 03/07/2018.
 */
public interface UserContract {

    @GetMapping("/users")
    List<UserResponse> findAll();

    @GetMapping("/users/{id}")
    UserResponse findOne(@PathVariable("id") String id);

    @DeleteMapping("/users/{id}")
    void delete(@PathVariable("id") String id);

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/users")
    UserResponse save(@RequestBody UserRequest request);

    @PutMapping("/users/{id}")
    void put(@PathVariable("id") String id,
             @RequestBody UserRequest request);
}
