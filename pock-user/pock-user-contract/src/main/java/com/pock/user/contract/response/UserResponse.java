package com.pock.user.contract.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Seven on 02/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class UserResponse {

    private String id;
    private String user;
    private String password;
}
