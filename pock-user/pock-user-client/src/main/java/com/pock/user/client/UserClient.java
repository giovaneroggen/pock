package com.pock.user.client;

import com.pock.user.contract.UserContract;
import com.pock.user.contract.response.UserResponse;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by Seven on 02/07/2018.
 */
@FeignClient(value = "http://USER-SERVICE", decode404 = true)
public interface UserClient extends UserContract{
}
