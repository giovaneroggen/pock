package com.pock.user.builder;

import com.pock.user.contract.request.UserRequest;
import com.pock.user.contract.response.UserResponse;
import com.pock.user.data.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class UserBuilderTest {

    @Test
    public void single_build_user_response() {
        User build = this.randomUser();
        UserResponse response = UserBuilder.buildUserResponse(build);
        this.assertUserEqualsToUserResponse(build, response);
    }

    @Test
    public void single_build_user_response_null_safe() {
        User build = null;
        assertNull(UserBuilder.buildUserResponse(build));
    }

    @Test
    public void list_build_user_response() {
        User build = this.randomUser();
        List<UserResponse> list = UserBuilder.buildUserResponse(Arrays.asList(build));
        assertNotNull(list);
        assertEquals(1, list.size());
        this.assertUserEqualsToUserResponse(build, list.get(0));
    }

    @Test
    public void list_build_user_response_empty_list() {
        List<UserResponse> list = UserBuilder.buildUserResponse(new ArrayList<>());
        assertNotNull(list);
        assertEquals(0, list.size());
    }

    @Test
    public void list_build_user_response_null_safe() {
        List<UserResponse> list = UserBuilder.buildUserResponse(new ArrayList<>());
        assertNotNull(list);
        assertEquals(0, list.size());
    }

    @Test
    public void build_from_request() {
        UserRequest build = this.randomUserRequest();
        User user = UserBuilder.buildFromRequest(build);
        assertNotNull(user);
        assertNull(user.getId());
        assertEquals(build.getUser(), user.getUser());
        assertEquals(build.getPassword(), user.getPassword());
    }

    @Test
    public void build_from_request_null_safe() {
        assertNull(UserBuilder.buildFromRequest(null));
    }

    @Test
    public void build_from_request_with_id() {
        UserRequest build = this.randomUserRequest();
        String id = UUID.randomUUID().toString();
        User user = UserBuilder.buildFromRequest(id, build);
        assertNotNull(user);
        assertEquals(id, user.getId());
        assertEquals(build.getUser(), user.getUser());
        assertEquals(build.getPassword(), user.getPassword());
    }

    @Test
    public void build_from_request_with_id_null_safe() {
        assertNull(UserBuilder.buildFromRequest(null, null));
    }

    private void assertUserEqualsToUserResponse(User build, UserResponse response) {
        assertNotNull(response);
        assertEquals(build.getId(), response.getId());
        assertEquals(build.getUser(), response.getUser());
        assertEquals(build.getPassword(), response.getPassword());
    }

    private User randomUser() {
        String id = UUID.randomUUID().toString();
        return User.builder()
                   .id(id)
                   .user("USER_"+id)
                   .password("PASSWORD_"+id)
                   .build();
    }

    private UserRequest randomUserRequest() {
        String id = UUID.randomUUID().toString();
        return UserRequest.builder()
                          .user("USER_"+id)
                          .password("PASSWORD_"+id)
                          .build();
    }
}