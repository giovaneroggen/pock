package com.pock.user.facade;

import com.pock.spring.web.config.exception.GenericException;
import com.pock.user.builder.UserBuilder;
import com.pock.user.contract.request.UserRequest;
import com.pock.user.contract.response.UserResponse;
import com.pock.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;
import static com.pock.user.builder.UserBuilder.*;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class UserFacade {

    @Autowired
    private UserService service;

    public List<UserResponse> findAll() {
        return buildUserResponse(this.service.findAll());
    }

    public UserResponse findOne(String id) {
        return this.service.findOne(id)
                   .map(UserBuilder::buildUserResponse)
                   .orElseThrow(() -> new GenericException(HttpStatus.NOT_FOUND));
    }

    public void delete(String id) {
        this.service.delete(id);
    }

    public UserResponse save(UserRequest request) {
        return buildUserResponse(this.service.save(buildFromRequest(request)));
    }

    public void save(String id, UserRequest request) {
        this.service.save(buildFromRequest(id, request));
    }
}
