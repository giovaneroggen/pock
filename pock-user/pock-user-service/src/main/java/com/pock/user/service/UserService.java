package com.pock.user.service;

import com.pock.user.data.User;
import com.pock.user.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public Optional<User> findOne(String id) {
        return this.repository.findOne(id);
    }

    public void delete(String id) {
        this.repository.delete(id);
    }

    public List<User> findAll() {
        return this.repository.findAll();
    }

    public User save(User user) {
        return this.repository.save(user);
    }
}
