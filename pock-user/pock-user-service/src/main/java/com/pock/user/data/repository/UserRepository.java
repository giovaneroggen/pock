package com.pock.user.data.repository;

import com.pock.user.data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by Seven on 04/07/2018.
 */
@Repository
public class UserRepository {

    private final static List<User> USER_LIST = new ArrayList<User>();

    public Optional<User> findOne(String id) {
        return USER_LIST.stream()
                        .filter(u -> u.getId().equals(id))
                        .findFirst();
    }

    public User save(User user) {
        if(Objects.nonNull(user.getId())){
            this.delete(user.getId());
        }else{
            user.setId(UUID.randomUUID().toString());
        }
        USER_LIST.add(user);
        return user;
    }

    public void delete(String id){
        USER_LIST.removeIf(u -> u.getId().equals(id));
    }

    public List<User> findAll() {
        return USER_LIST;
    }
}
