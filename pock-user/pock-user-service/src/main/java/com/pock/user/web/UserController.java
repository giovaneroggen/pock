package com.pock.user.web;

import com.pock.user.contract.UserContract;
import com.pock.user.contract.request.UserRequest;
import com.pock.user.contract.response.UserResponse;
import com.pock.user.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.pock.user.builder.UserBuilder.buildFromRequest;
import static com.pock.user.builder.UserBuilder.buildUserResponse;

/**
 * Created by Seven on 03/07/2018.
 */
@RestController
public class UserController implements UserContract{

    @Autowired
    private UserFacade facade;

    @Override
    public List<UserResponse> findAll() {
        return this.facade.findAll();
    }

    @Override
    public UserResponse findOne(@PathVariable("id") String id) {
        return this.facade.findOne(id);
    }

    @Override
    public void delete(@PathVariable("id") String id) {
        this.facade.delete(id);
    }

    @Override
    public UserResponse save(@RequestBody UserRequest request) {
        return this.facade.save(request);
    }

    @Override
    public void put(@PathVariable("id") String id, @RequestBody UserRequest request) {
        this.facade.save(id, request);
    }
}
