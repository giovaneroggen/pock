package com.pock.user.builder;

import com.pock.user.contract.request.UserRequest;
import com.pock.user.contract.response.UserResponse;
import com.pock.user.data.User;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by Seven on 04/07/2018.
 */
public class UserBuilder {

    public static UserResponse buildUserResponse(User user) {
        return Optional.ofNullable(user)
                       .map(u ->
                                UserResponse.builder()
                                            .id(u.getId())
                                            .user(u.getUser())
                                            .password(u.getPassword())
                                            .build())
                        .orElse(null);
    }

    public static List<UserResponse> buildUserResponse(List<User> list) {
        return Optional.ofNullable(list)
                       .map(l ->
                               l.stream()
                                .map(UserBuilder::buildUserResponse)
                                .collect(Collectors.toList()))
                       .orElse(null);
    }

    public static User buildFromRequest(UserRequest request) {
        return Optional.ofNullable(request)
                       .map(r ->
                               User.builder()
                                       .user(r.getUser())
                                       .password(r.getPassword())
                                       .build())
                       .orElse(null);
    }

    public static User buildFromRequest(String id, UserRequest request) {
        return Optional.ofNullable(request)
                       .map(r ->
                               User.builder()
                                       .id(id)
                                       .user(r.getUser())
                                       .password(r.getPassword())
                                       .build())
                       .orElse(null);
    }
}
