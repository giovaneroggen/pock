package com.pock.user.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Seven on 04/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class User {
    private String id;
    private String user;
    private String password;
}
