package com.pock.product.client;

import com.pock.product.contract.ProductContract;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * Created by Seven on 04/07/2018.
 */
@FeignClient(value = "http://PRODUCT-SERVICE")
public interface ProductClient extends ProductContract{
}
