package com.pock.product.facade;

import com.pock.product.builder.ProductBuilder;
import com.pock.product.contract.request.ProductRequest;
import com.pock.product.contract.response.ProductResponse;
import com.pock.product.service.ProductService;
import com.pock.spring.web.config.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.pock.product.builder.ProductBuilder.buidProductResponse;
import static com.pock.product.builder.ProductBuilder.buildFromRequest;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class ProductFacade {

    @Autowired
    private ProductService service;

    public List<ProductResponse> findAll() {
        return buidProductResponse(this.service.findAll());
    }

    public ProductResponse save(ProductRequest request) {
        return buidProductResponse(this.service.save(buildFromRequest(request)));
    }

    public ProductResponse findOne(String id) {
        return this.service.findOne(id)
                   .map(ProductBuilder::buidProductResponse)
                   .orElseThrow(() -> new GenericException(HttpStatus.NOT_FOUND));
    }

    public void delete(String id) {
        this.service.delete(id);
    }
}
