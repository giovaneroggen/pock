package com.pock.product.data.repository;

import com.pock.product.data.Product;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Seven on 04/07/2018.
 */
@Repository
public class ProductRepository {

    private final static List<Product> PRODUCT_LIST = new ArrayList<Product>();

    public Optional<Product> findOne(String id) {
        return PRODUCT_LIST.stream()
                           .filter(u -> u.getId().equals(id))
                           .findFirst();
    }

    public List<Product> findAll(){
        return PRODUCT_LIST;
    }

    public Product save(Product product) {
        if(Objects.nonNull(product.getId())){
            this.delete(product.getId());
        }else{
            product.setId(UUID.randomUUID().toString());
        }
        PRODUCT_LIST.add(product);
        return product;
    }

    public void delete(String id){
        PRODUCT_LIST.removeIf(u -> u.getId().equals(id));
    }
}
