package com.pock.product.builder;

import com.pock.product.contract.request.ProductRequest;
import com.pock.product.contract.response.ProductResponse;
import com.pock.product.data.Product;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Seven on 04/07/2018.
 */
public class ProductBuilder {

    public static List<ProductResponse> buidProductResponse(List<Product> list) {
        return list.stream()
                   .map(ProductBuilder::buidProductResponse)
                   .collect(Collectors.toList());
    }

    public static ProductResponse buidProductResponse(Product product) {
        return ProductResponse.builder()
                              .id(product.getId())
                              .name(product.getName())
                              .price(product.getPrice())
                              .build();
    }

    public static Product buildFromRequest(ProductRequest request) {
        return Product.builder()
                      .name(request.getName())
                      .price(request.getPrice())
                      .build();
    }
}
