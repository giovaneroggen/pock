package com.pock.product.service;

import com.pock.product.data.Product;
import com.pock.product.data.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public List<Product> findAll() {
        return this.repository.findAll();
    }

    public Product save(Product product) {
        return this.repository.save(product);
    }

    public Optional<Product> findOne(String id) {
        return this.repository.findOne(id);
    }

    public void delete(String id) {
        this.repository.delete(id);
    }
}
