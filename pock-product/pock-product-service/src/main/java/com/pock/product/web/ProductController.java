package com.pock.product.web;

import com.pock.product.contract.ProductContract;
import com.pock.product.contract.request.ProductRequest;
import com.pock.product.contract.response.ProductResponse;
import com.pock.product.facade.ProductFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Seven on 04/07/2018.
 */
@RestController
public class ProductController implements ProductContract{

    @Autowired
    private ProductFacade facade;

    @Override
    public List<ProductResponse> findAll() {
        return this.facade.findAll();
    }

    @Override
    public ProductResponse save(@RequestBody ProductRequest request) {
        return this.facade.save(request);
    }

    @Override
    public ProductResponse findOne(@PathVariable("id") String id) {
        return this.facade.findOne(id);
    }

    @Override
    public void delete(@PathVariable("id") String id) {
        this.facade.delete(id);
    }
}
