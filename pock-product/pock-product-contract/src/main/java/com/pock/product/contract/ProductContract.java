package com.pock.product.contract;

import com.pock.product.contract.request.ProductRequest;
import com.pock.product.contract.response.ProductResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Seven on 04/07/2018.
 */
public interface ProductContract {

    @GetMapping("/products")
    public List<ProductResponse> findAll();

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/products")
    public ProductResponse save(@RequestBody ProductRequest request);

    @GetMapping("/products/{id}")
    public ProductResponse findOne(@PathVariable("id") String id);

    @DeleteMapping("/products/{id}")
    public void delete(@PathVariable("id") String id);
}
