package com.pock.basket.client;

import com.pock.basket.contract.BasketContract;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * Created by Seven on 04/07/2018.
 */
@FeignClient(value = "http://BASKET-SERVICE")
public interface BasketClient extends BasketContract{
}
