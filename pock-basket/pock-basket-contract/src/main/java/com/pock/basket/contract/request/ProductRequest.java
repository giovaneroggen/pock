package com.pock.basket.contract.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by Seven on 04/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class ProductRequest {
    private String productId;
    private Long quantity;
}
