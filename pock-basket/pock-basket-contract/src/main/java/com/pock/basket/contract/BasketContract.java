package com.pock.basket.contract;

import com.pock.basket.contract.request.ProductRequest;
import com.pock.basket.contract.response.BasketResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Seven on 04/07/2018.
 */
public interface BasketContract {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/baskets/users/{userId}")
    BasketResponse create(@PathVariable("userId") String userId);

    @GetMapping("/baskets/users/{userId}")
    List<BasketResponse> findAllByUserId(@PathVariable("userId") String userId);

    @GetMapping("/baskets/{id}")
    BasketResponse findOne(@PathVariable("id") String id);

    @DeleteMapping("/baskets/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/baskets/{id}/products")
    BasketResponse addProduct(@PathVariable("id") String id,
                              @RequestBody ProductRequest request);

    @DeleteMapping("/baskets/{id}/products/{productId}")
    BasketResponse deleteProduct(@PathVariable("id") String id,
                                 @PathVariable("productId") String productId);
}
