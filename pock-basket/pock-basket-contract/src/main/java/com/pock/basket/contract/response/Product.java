package com.pock.basket.contract.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by Seven on 04/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class Product {
    private String id;
    private Long quantity;
    private String name;
    private BigDecimal price;
    private BigDecimal totalPrice;
}
