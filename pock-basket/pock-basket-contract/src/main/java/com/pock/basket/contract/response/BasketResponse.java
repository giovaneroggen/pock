package com.pock.basket.contract.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Seven on 04/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class BasketResponse {
    private String id;
    private String userId;
    private BigDecimal total;
    private List<Product> productList;
}
