package com.pock.basket.web;

import com.pock.basket.contract.BasketContract;
import com.pock.basket.contract.request.ProductRequest;
import com.pock.basket.contract.response.BasketResponse;
import com.pock.basket.facade.BasketFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Seven on 04/07/2018.
 */
@RestController
public class BasketController implements BasketContract {

    @Autowired
    private BasketFacade facade;

    @Override
    public BasketResponse create(@PathVariable("userId") String userId) {
        return this.facade.create(userId);
    }

    @Override
    public List<BasketResponse> findAllByUserId(@PathVariable("userId") String userId) {
        return this.facade.findAllByUserId(userId);
    }

    @Override
    public BasketResponse findOne(@PathVariable("id") String id) {
        return this.facade.findOne(id);
    }

    @Override
    public void delete(@PathVariable("id") String id) {
        this.facade.delete(id);
    }

    @Override
    public BasketResponse addProduct(@PathVariable("id") String id, @RequestBody ProductRequest request) {
        return this.facade.addProduct(id, request);
    }

    @Override
    public BasketResponse deleteProduct(@PathVariable("id") String id, @PathVariable("productId") String productId) {
        return this.facade.deleteProduct(id, productId);
    }
}
