package com.pock.basket.service;

import com.pock.basket.data.Basket;
import com.pock.basket.data.repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Service
public class BasketService {

    @Autowired
    private BasketRepository repository;

    public Basket create(String userId) {
        return this.repository.save(Basket.builder()
                                          .userId(userId)
                                          .build());
    }

    public List<Basket> findAllByUserId(String userId){
        return this.repository.findAllByUserId(userId);
    }

    public Optional<Basket> findOne(String id) {
        return this.repository.findOne(id);
    }

    public void delete(String id) {
        this.repository.delete(id);
    }
}
