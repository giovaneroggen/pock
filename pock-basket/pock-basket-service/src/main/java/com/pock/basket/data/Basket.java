package com.pock.basket.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.HashMap;

/**
 * Created by Seven on 04/07/2018.
 */
@Data
@Builder
@AllArgsConstructor
public class Basket {

    private String id;
    private String userId;
    @Builder.Default
    private HashMap<String, Long> productIdToQuantity = new HashMap<>();
}
