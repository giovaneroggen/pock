package com.pock.basket.facade;

import com.pock.product.client.ProductClient;
import com.pock.product.contract.response.ProductResponse;
import com.pock.spring.web.config.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class ProductFacade {

    @Autowired
    private ProductClient productClient;

    public ProductResponse findRequiredProduct(String id) {
        return Optional.ofNullable(this.productClient.findOne(id))
                       .orElseThrow(() -> new GenericException("product_not_found", HttpStatus.NOT_FOUND));
    }
}
