package com.pock.basket.facade;

import com.pock.basket.builder.BasketBuilder;
import com.pock.basket.contract.request.ProductRequest;
import com.pock.basket.contract.response.BasketResponse;
import com.pock.basket.data.Basket;
import com.pock.basket.service.BasketService;
import com.pock.spring.web.config.exception.GenericException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class BasketFacade {

    @Autowired
    private BasketService basketService;

    @Autowired
    private UserFacade userFacade;

    @Autowired
    private BasketBuilder basketBuilder;

    public BasketResponse create(String userId) {
        this.isValidUser(userId);
        return this.basketBuilder.buildBasketResponse(
                this.basketService.create(userId)
        );
    }

    public List<BasketResponse> findAllByUserId(String userId) {
        this.isValidUser(userId);
        return this.basketBuilder.buildBasketResponse(
                this.basketService.findAllByUserId(userId)
        );
    }

    private void isValidUser(String userId) {
        this.userFacade.findRequiredUser(userId);
    }

    public BasketResponse findOne(String id) {
        return this.basketBuilder.buildBasketResponse(this.findOneRequired(id));
    }

    public void delete(String id) {
        this.basketService.delete(id);
    }

    public BasketResponse addProduct(String id, ProductRequest request) {
        final Basket basket = this.findOneRequired(id);
        this.basketBuilder.findRequiredProduct(request.getProductId());
        final String productId = request.getProductId();
        Long quantityOnBase = this.productQuantityOnBasket(productId, basket);
        final long realQuantity = request.getQuantity() + quantityOnBase;
        basket.getProductIdToQuantity().put(productId, realQuantity);
        return this.basketBuilder.buildBasketResponse(basket);
    }

    private long productQuantityOnBasket(String productId, Basket basket) {
        final HashMap<String, Long> map = basket.getProductIdToQuantity();
        if(map.containsKey(productId)) {
            return Optional.ofNullable(map.get(productId))
                           .orElse(0L);
        }
        return 0L;
    }

    private Basket findOneRequired(String id) {
        return this.basketService.findOne(id)
                .orElseThrow(() -> new GenericException("basket_not_found", HttpStatus.NOT_FOUND));
    }

    public BasketResponse deleteProduct(String id, String productId) {
        final Basket basket = this.findOneRequired(id);
        basket.getProductIdToQuantity().remove(productId);
        return this.basketBuilder.buildBasketResponse(basket);
    }
}
