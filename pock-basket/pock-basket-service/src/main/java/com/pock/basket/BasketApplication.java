package com.pock.basket;

import com.pock.product.client.ProductClient;
import com.pock.user.client.UserClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Created by Seven on 04/07/2018.
 */
@EnableEurekaClient
@SpringBootApplication
@EnableFeignClients(clients = {ProductClient.class, UserClient.class})
public class BasketApplication {

    public static void main(String[] args) {
        SpringApplication.run(BasketApplication.class, args);
    }
}
