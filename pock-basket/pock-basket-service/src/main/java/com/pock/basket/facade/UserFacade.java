package com.pock.basket.facade;

import com.pock.spring.web.config.exception.GenericException;
import com.pock.user.client.UserClient;
import com.pock.user.contract.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class UserFacade {

    @Autowired
    private UserClient userClient;

    public UserResponse findRequiredUser(String userId) {
        return Optional.ofNullable(this.userClient.findOne(userId))
                       .orElseThrow(() -> new GenericException("user_not_found", HttpStatus.NOT_FOUND));
    }
}
