package com.pock.basket.data.repository;

import com.pock.basket.data.Basket;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Seven on 04/07/2018.
 */
@Repository
public class BasketRepository {

    private final static List<Basket> BASKET_LIST = new ArrayList<Basket>();

    public Optional<Basket> findOne(String id) {
        return BASKET_LIST.stream()
                          .filter(u -> u.getId().equals(id))
                          .findFirst();
    }

    public List<Basket> findAllByUserId(String userId){
        return BASKET_LIST.stream()
                           .filter(u -> u.getUserId().equals(userId))
                           .collect(Collectors.toList());
    }

    public Basket save(Basket basket) {
        if(Objects.nonNull(basket.getId())){
            this.delete(basket.getId());
        }else{
            basket.setId(UUID.randomUUID().toString());
        }
        BASKET_LIST.add(basket);
        return basket;
    }

    public void delete(String id){
        BASKET_LIST.removeIf(u -> u.getId().equals(id));
    }
}
