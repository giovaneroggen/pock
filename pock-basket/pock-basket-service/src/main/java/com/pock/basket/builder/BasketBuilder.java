package com.pock.basket.builder;

import com.pock.basket.contract.response.BasketResponse;
import com.pock.basket.contract.response.Product;
import com.pock.basket.data.Basket;
import com.pock.basket.facade.ProductFacade;
import com.pock.product.contract.response.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Seven on 04/07/2018.
 */
@Component
public class BasketBuilder {

    @Autowired
    private ProductFacade productFacade;

    public BasketResponse buildBasketResponse(Basket basket) {
        List<Product> productList = this.buildProduct(basket.getProductIdToQuantity());
        return BasketResponse.builder()
                             .id(basket.getId())
                             .userId(basket.getUserId())
                             .productList(productList)
                             .total(this.buildBasketTotal(productList))
                             .build();
    }

    private BigDecimal buildBasketTotal(List<Product> productList) {
        return productList
                    .stream()
                    .map(Product::getTotalPrice)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private List<Product> buildProduct(HashMap<String, Long> products) {
        return products.entrySet()
                       .stream()
                       .map(it -> this.buildProduct(it.getKey(), it.getValue()))
                       .collect(Collectors.toList());
    }

    private Product buildProduct(String id, Long quantity) {
        final ProductResponse product = this.findRequiredProduct(id);
        return Product.builder()
                      .id(id)
                      .quantity(quantity)
                      .name(product.getName())
                      .price(product.getPrice())
                      .totalPrice(BigDecimal.valueOf(quantity).multiply(product.getPrice()))
                      .build();
    }

    public ProductResponse findRequiredProduct(String id) {
        return this.productFacade.findRequiredProduct(id);
    }

    public List<BasketResponse> buildBasketResponse(List<Basket> list) {
        return list.stream()
                   .map(this::buildBasketResponse)
                   .collect(Collectors.toList());
    }
}
